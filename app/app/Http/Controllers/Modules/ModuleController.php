<?php

namespace App\Http\Controllers\Modules;

use Illuminate\Support\Facades\Input;

use App\Entities\Module;

use App\Http\Controllers\Controller;

class ModuleController extends Controller {

    public function index() {
        $accounts = ( array ) Input::get( 'account', [] );
        $per_page = ( int ) Input::get( 'per_page', 0 );
        $page = ( int ) Input::get( 'page', 0 );

		return response()->json( [
		    'result' => 'successful',
             'taxonomies' => Taxonomy::get_taxonomies( 'Module' ),
             'modules' => Module::get_modules( $accounts, $per_page, $page )
        ] );
    }
}