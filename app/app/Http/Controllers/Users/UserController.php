<?php

namespace App\Http\Controllers\Users;

use Illuminate\Support\Facades\Input;

use App\Entities\User;

use App\Http\Controllers\Controller;

class UserController extends Controller {

    public function index() {
        $accounts = ( array ) Input::get( 'account', [] );
        $per_page = ( int ) Input::get( 'per_page', 0 );
        $page = ( int ) Input::get( 'page', 0 );

		return response()->json( [
		    'result' => 'successful',
            'users' => User::get_users( $accounts, $per_page, $page )
        ] );
    }

}