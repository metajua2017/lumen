<?php

namespace App\Entities;

class Account {

	public $id;
	public $name;

	public function __construct( $identifier ) {
		$term = new Term( 'Account', $identifier );

		if ( $term->id != null ) :

			$this->id = $term->id;
			$this->name = $term->name;

		endif;
	}

	public static function add( $name = '', $user_id = null ) {
		if ( empty( $name ) )
			return null;

		return Term::add( 'Account', $name, $user_id );
	}

}